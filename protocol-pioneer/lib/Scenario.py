# pyright: reportInvalidStringEscapeSequence=false

from .model import *


import time
from ipycanvas import Canvas, hold_canvas
from typing import Callable, List
from dataclasses import dataclass

@dataclass
class Scenario:
    """Base class for scenarios.
    
    """

    def __init__(self,
        world_grid_height: int,
        world_grid_width: int,
        grid_pixel_size: int,
        canvas_pixel_padding: int = 0,
        canvas_pixel_width: int = 0,
        canvas_pixel_height: int = 0,
        wait_for_user: bool = False,
        animation_frames: int = 10,
        message_filter: Callable[[str], bool] = lambda x: True
                 ):
        """canvas_pixel_width and height will be auto-calculated if they are 0.
        
        wait_for_user will pause the scenario every round until the user presses enter.
        """

        self.wait_for_user = wait_for_user
        self.animation_frames = animation_frames
        
        if canvas_pixel_height == 0:
            canvas_pixel_height = (world_grid_height * grid_pixel_size) + (canvas_pixel_padding * 2)
        if canvas_pixel_width == 0:
            canvas_pixel_width = (world_grid_width * grid_pixel_size) + (canvas_pixel_padding * 2)

        self.world = World(
            world_height=world_grid_height,
            world_width=world_grid_width,
            grid_size=grid_pixel_size,
            canvas_padding=canvas_pixel_padding,
            message_filter=message_filter
        )

        # set up canvas
        self.canvas = Canvas(width=canvas_pixel_width, height=canvas_pixel_height)
        # display will be defined in the notebook
        display(self.canvas) # pyright: ignore


    def win(self) -> bool:
        """Scenarios should override this. Returns true if the user has won."""
        return False

    def lose(self) -> bool:
        """Scenarios should override this. Returns true if the user has lost."""
        return False

    def run(self) -> None:


        turn = 0
        while not self.win() and not self.lose():
            turn += 1
            # movable_entities = len([e for e in self.world.entities if e.movable])
            # print(f"Turn {turn}. {movable_entities} movables to render.")
            # display world
            self.world.draw_all(canvas=self.canvas)
            
            if self.wait_for_user:
                input("Press Enter to continue...")
            else:
                time.sleep(0.5 * self.animation_frames / 10)

            # Update the world on tick
            self.world.process_all()
            
            # Animate the update (use 10 frames)
            for i in range(1, self.animation_frames + 1):
                with hold_canvas(self.canvas):
                    self.canvas.clear()
                    self.world.animate_all(canvas=self.canvas, percent=i/self.animation_frames)
                    time.sleep(0.1)
            
            self.world.post_animate_all()
            
        if self.win():
            self.post_win()
        elif self.lose():
            self.post_lose()

    def post_win(self) -> None:
        """Scenarios should override this."""
        print("You win!")

    def post_lose(self) -> None:
        """Scenarios should override this."""
        print("You lose!")

    @staticmethod
    def introduce():
        """Introduce the scenario to the user."""
        print(
            """
    ____             __                   ______  _                           
   / __ \_________  / /_____  _________  / / __ \(_)___  ____  ___  ___  _____
  / /_/ / ___/ __ \/ __/ __ \/ ___/ __ \/ / /_/ / / __ \/ __ \/ _ \/ _ \/ ___/
 / ____/ /  / /_/ / /_/ /_/ / /__/ /_/ / / ____/ / /_/ / / / /  __/  __/ /    
/_/   /_/   \____/\__/\____/\___/\____/_/_/   /_/\____/_/ /_/\___/\___/_/  

"""
        )

class Scenario1(Scenario):

    def __init__(self,
                 user_strategy:Callable[[UserNode], None],
                 wait_for_user: bool = False,
                 animation_frames: int = 10
                 ) -> None:
        

        # I'll track the win condition using a member variable,
        # and create a set_win function to use in a closure below.
        self.did_win = False
        def set_win(v: bool) -> None:
            self.did_win = v

        # Emitter node strategy
        def emitter_strategy(self):
            """This "Emitter" node emits messages every x clicks."""
            
            x = 5
            message = "Mothership Aether transmitting in the blind... Any unit still responsive please respond 'Message Received'..."

            # handle received message
            while(self.message_queue):
                m: QueuedMessage = self.message_queue.pop()
                if m.text == "Message Received":
                    # win
                    set_win(True)
                else:
                    message = f"Negative copy. We received the message '{m.text}' Please respond 'Message Received' is you are receiving our message."


            # create a variable to keep track of clicks. Reset to 0 after x.
            if "clicks" not in self.state:
                self.state["clicks"] = 0
            else:
                self.state["clicks"] = (self.state["clicks"] + 1) % x

            if self.state["clicks"] == 0:
                text = message
                interface = "E"
                self.send_message(text, interface)

        strategies = {
            "A": emitter_strategy,
            "B": user_strategy
        }

        sprites = {
            "A": "assets/min/Mothership1.png",
            "B": "assets/min/Damaged-Ship-1.png",
        }

        ids = {
            "A": "Mothership",
            "B": "You"
        }

        # Load entities and tiles
        tile_map: List[str] = [
            "........",
            "......|.",
            ".A----B.",
            "......|.",
            "........",
        ]

        # Create world
        super().__init__(
            world_grid_height=len(tile_map),
            world_grid_width=len(tile_map[0]),
            grid_pixel_size=50,
            wait_for_user=wait_for_user,
            animation_frames=animation_frames
        )

        for (y, row) in enumerate(tile_map):
            for (x, t) in enumerate(row):
                if t == ".":
                    # self.world.add_tile(Blank_Tile(grid_x = x,grid_y = y, world = self.world))
                    pass
                elif t in "ABCDE":
                    self.world.add_entity(
                        UserNode(
                            grid_x= x,
                            grid_y= y,
                            world=self.world,
                            id=ids[t],
                            label=ids[t],
                            process_strategy=strategies[t],
                            sprite_file=sprites[t]
                        ))
                elif t == "-":
                    self.world.add_tile(EW_Wire(grid_x=x, grid_y=y, world=self.world))
                elif t == "|":
                    self.world.add_tile(NS_Wire(grid_x=x, grid_y=y, world=self.world))
                elif t == "^":
                    self.world.add_tile(NEW_Wire(grid_x=x, grid_y=y, world=self.world))

    def win(self) -> bool:
        return self.did_win

    @staticmethod
    def introduce():
        Scenario.introduce()
        print(
            """
Chapter 1: A Broken World

"... respond 'Message Received' <static. bleep bloop> Mothership Aether transmitting in the blind... Any unit still responsive please respond 'Message Received' <static. bleep bloop> Mothership Aether... " 

The radio repeats the message. Dim red emergency lights illuminate the comms room. The smell of burning electrical boxes singe your nostrils.

"Holy shit!" says a voice behind you. You turn your head painfully and see the damaged smart speaker through the smoke. Alexa goes on: "I think we were hit by a space missile!"

You glance at the communication panel, the static from the radio mixing with the echoing alarms that fill the room.

"Get to the Echo module and see what you can salvage," you bark to a figure crumpled beneath a radar console. In the dim light you can barely recognize the figure as as Lieutenant Jansen, someone you are finally starting to call a friend. "I'll try to send a distress signal to the nearest space station. We need help, now!"

As Jansen nods and scurries to the Echo module, you swivel back to the communication panel. Your fingers dance across the keys as you attempt to bypass the damaged circuits to send a distress signal. The smoke in the room grows thicker, stinging your eyes, but you push through the discomfort, focusing.
"""
        )

    def post_win(self) -> None:
        print(
            """
The radio crackles to life. "Message Received. This is the USS Aether. We are sending a repair party your way. Hold tight."

Jansen returns, a tiny light blinking in his hand. "Got some data," he says, shaken but firm. You nod, looking around the dim room. Outside, the vastness of space; inside, a tight, broken space.

~~ Start Chapter 2. ~~
"""
        )