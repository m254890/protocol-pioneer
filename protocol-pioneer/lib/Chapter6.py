# pyright: reportInvalidStringEscapeSequence=false

from .Scenario import Scenario
from .model import *
from random import randint, shuffle

from typing import Callable, List, Union

class Chapter6(Scenario):

    def __init__(self,
                 drone_strategy:Callable[[UserNode], None],
                 player_strategy:Callable[[UserNode], None],
                 wait_for_user: bool = False,
                 animation_frames: int = 10,
                 message_filter: Callable[[str], bool] = lambda x: True
                 ) -> None:
        
        # Load entities and tiles
        tile_map: List[str] = [
            "...1.....2......",
            "...|.....|......",
            "...|..x--x--x...",
            "...|..|.....|...",
            ".5-x--x..0..x-3.",
            "...|..|..|..|...",
            ".6-x--x--x--x-4.",
            "...|..|..|..|...",
            ".7-x--x--+--x...",
            "...|.....|......",
            "...8.....9......",
            "......l.........",
        ]

        # Win/lose setup
        self.did_win = False
        self.did_lose = False

        # In this chapter, win_conditions keeps track of successful radio transmissions. We're looking for a
        # certain rate of successful transmissions over the last 20 ticks.
        self.win_conditions: List[int] = [] # This is a list of ints. Each int is the tick in which a successful radio transmission occurred.
        # Possible lose conditions:
        # "wrong key" -> Sawblade puts a wrong or expired key into the radio.
        self.lose_condition: str = ""

        def set_win(i) -> None:
            if i == "radio_success":
                self.win_conditions.append(self.world.get_current_tick())
                max_ticks = min([50, self.world.get_current_tick()])
                # Give the player the current success rate over the last 50 ticks
#                 print(f"""
# Successes: {len([x for x in self.win_conditions if x >= self.world.get_current_tick() - 50])} in last {max_ticks} ticks.")
# Rate: {len([x for x in self.win_conditions if x >= self.world.get_current_tick() - 50]) / max_ticks}
# """)
                
        def get_label() -> str:
            max_ticks = min([50, self.world.get_current_tick()])
            msg = f"Successes: {len([x for x in self.win_conditions if x >= self.world.get_current_tick() - 50])} in last {max_ticks} ticks. "
            msg += f"Rate: {round(len([x for x in self.win_conditions if x >= self.world.get_current_tick() - 50]) / max_ticks, 2)}"
            return msg
        
        def set_lose(i) -> None:
            self.lose_condition = i
            self.did_lose = True

        # Each key is a tuple (key, expiration date, id)
        self.radio_keys: List[Tuple[int, int, str]] = []
        def get_new_key() -> Tuple[int, int, str]:
            """This keeps track of the key and expiration date that the player generates."""
            # Find the entities that are PlayerNodes
            entity_ids = [x.id for x in self.world.entities if isinstance(x, PlayerNode)]
            new_key = (randint(0, 100), self.world.get_current_tick() + 20, random.choice(entity_ids))
            self.radio_keys.append(new_key)
            return new_key
        
        def check_key(key, id) -> bool:
            """This checks if a key is valid, not expired, and for the correct id."""
            matched_keys = [x for x in self.radio_keys if
                            x[0] == key and
                            x[1] >= self.world.get_current_tick() and
                            x[2] == id]
            if len(matched_keys) > 0:
                return True
            else:
                print(f"Current tick: {self.world.get_current_tick()}. Key: {key}. ID: {id}. Not valid.")
                return False

        @dataclass(kw_only=True)
        class LimitedUserNode(UserNode):
            """This node is limited to sending one message per interface per turn."""
            messages_sent_this_turn: Dict[str, int] = field(default_factory=dict)
            message_cap: int = 1

            def process(self):
                self.messages_sent_this_turn = {"N": 0, "S": 0, "E": 0, "W": 0}
                return super().process()
            
            def send_message(self, message_text: str, interface: str) -> None:
                if self.messages_sent_this_turn[interface] >= self.message_cap:
                    print(f"Error detected at {self.id}: Message jam. You can only send {self.message_cap} messages per interface per turn.)")
                else:
                    self.messages_sent_this_turn[interface] += 1
                    super().send_message(message_text, interface)

        @dataclass(kw_only=True)
        class PlayerNode(LimitedUserNode):
            """This node represents the player, floating in space."""
            def generate_key(self) -> Tuple[int, int, str]:
                return get_new_key()
            
            def operate_radio(self, key: int | str) -> None:
                # cast key to int if it is a string
                if isinstance(key, str):
                    key = int(key)

                if check_key(key, self.id):
                    set_win("radio_success")
                else:
                    set_lose("wrong key")
            

        def label_strategy(self):
            """This strategy is for the labels that appear on the screen."""
            self.label = get_label()


        strategies = {
            "1": player_strategy,
            "2": player_strategy,
            "3": player_strategy,
            "4": player_strategy,
            "5": player_strategy,
            "6": player_strategy,
            "7": player_strategy,
            "8": player_strategy,
            "9": player_strategy,
            "0": player_strategy,
            "x": drone_strategy,
        }

        sprites = {
            "1": "assets/min/battlebot1.png",
            "2": "assets/min/battlebot1.png",
            "3": "assets/min/battlebot1.png",
            "4": "assets/min/battlebot1.png",
            "5": "assets/min/battlebot1.png",
            "6": "assets/min/battlebot1.png",
            "7": "assets/min/battlebot1.png",
            "8": "assets/min/battlebot1.png",
            "9": "assets/min/battlebot1.png",
            "0": "assets/min/battlebot1.png",
            "x": "assets/min/Drone-1.png",
        }
        
        # Create world
        super().__init__(
            world_grid_height=len(tile_map),
            world_grid_width=len(tile_map[0]),
            grid_pixel_size=50,
            wait_for_user=wait_for_user,
            animation_frames=animation_frames,
            message_filter=message_filter
        )

        for (y, row) in enumerate(tile_map):
            for (x, t) in enumerate(row):
                if t == ".":
                    pass
                elif t in "1234567890":
                    self.world.add_entity(
                        PlayerNode(
                            grid_x= x,
                            grid_y= y,
                            world=self.world,
                            id=t,
                            label=f"BattleDroid {t}",
                            process_strategy=strategies[t],
                            sprite_file=sprites[t],
                            message_cap=2
                        ))
                elif t in "x":
                    # The id for the drone should be a random 5-character string (all capital letters)
                    id = "".join([chr(randint(65, 90)) for _ in range(5)])
                    self.world.add_entity(
                        LimitedUserNode(
                            grid_x= x,
                            grid_y= y,
                            world=self.world,
                            id=id,
                            label=f"Drone {id}",
                            process_strategy=strategies[t],
                            sprite_file=sprites[t],
                            message_cap=2
                        ))
                elif t in "l":
                    # Label
                    self.world.add_entity(
                        DrawableText(
                            grid_x= x,
                            grid_y= y,
                            world=self.world,
                            id="label1",
                            process_strategy=label_strategy,
                        ))
                elif t == "-":
                    self.world.add_tile(EW_Wire(grid_x=x, grid_y=y, world=self.world))
                elif t == "|":
                    self.world.add_tile(NS_Wire(grid_x=x, grid_y=y, world=self.world))
                elif t == "⅃":
                    self.world.add_tile(NW_Wire(grid_x=x, grid_y=y, world=self.world))
                elif t == "+":
                    self.world.add_tile(NSEW_Wire(grid_x=x, grid_y=y, world=self.world))
        
    @staticmethod
    def introduce():
        print(
"""
    ____             __                   ______  _                           
   / __ \_________  / /_____  _________  / / __ \(_)___  ____  ___  ___  _____
  / /_/ / ___/ __ \/ __/ __ \/ ___/ __ \/ / /_/ / / __ \/ __ \/ _ \/ _ \/ ___/
 / ____/ /  / /_/ / /_/ /_/ / /__/ /_/ / / ____/ / /_/ / / / /  __/  __/ /    
/_/   /_/   \____/\__/\____/\___/\____/_/_/   /_/\____/_/ /_/\___/\___/_/
              
Chapter 6
""")

    def win(self) -> bool:
        return self.did_win
    
    def lose(self) -> bool:
        return self.did_lose
    
    def post_win(self) -> None:
        print(
"""

You Win!

"""
        )
    
    def post_lose(self) -> None:
        if self.lose_condition == "wrong key":
            print(
"""
GAME OVER. TRY AGAIN?
"""
        )