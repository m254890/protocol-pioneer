
from Scenario import Scenario
from typing import Callable, List
from model import *


class BasicRoutingScenario(Scenario):

    def __init__(self, router_process_strategy:Callable[[UserNode], None]) -> None:
        
        tile_map: List[str] = [
            ".....D....", # 1
            ".....|....", # 2
            ".....|....", # 3
            ".....|....", # 4
            "A----B--E", # 5
            ".....|....", # 6
            ".....|....", # 7
            ".....|....", # 8
            ".....C....", # 9
            "..........", # 10
            #0123456789
        ]


        def process_strategy_a(self):
            """This "Emitter" node emits messages every x clicks."""
            
            x = 3

            # create a variable to keep track of clicks. Reset to 0 after x.
            if "clicks" not in self.state:
                self.state["clicks"] = 0
            else:
                self.state["clicks"] = (self.state["clicks"] + 1) % x

            if self.state["clicks"] == 0:
                text = "Hello!"
                interface = "E"
                self.send_message(text, interface)

        def process_strategy_c(self):
            """Receiver Node."""
            while(self.message_queue):
                msg: QueuedMessage = self.message_queue.pop()
                print(f"{self.id} (addr: {self.state['address']}): Received {msg.text} on interface {msg.interface}!")

            



        # Create world
        super().__init__(
            world_grid_height=len(tile_map),
            world_grid_width=len(tile_map[0]),
            grid_pixel_size=50,
        )


        # Load entities and tiles
        for (y, row) in enumerate(tile_map):
            for (x, t) in enumerate(row):
                if t == ".":
                    # world.add_tile(Blank_Tile(grid_x = x,grid_y = y, world = self))
                    pass
                elif t == "A":
                    self.world.add_entity(
                        UserNode(
                            grid_x= x,
                            grid_y= y,
                            world=self.world,
                            id="Emitter",
                            process_strategy=process_strategy_a
                        ))
                elif t == "B":
                    self.world.add_entity(
                        UserNode(
                            grid_x= x,
                            grid_y= y,
                            world=self.world,
                            id="Router",
                            process_strategy=router_process_strategy
                        ))
                elif t in "CDE":
                    ips = {
                        "C": "192.168.0.1",
                        "D": "192.168.0.2",
                        "E": "192.168.0.3",
                    }
                    self.world.add_entity(
                        UserNode(
                            grid_x= x,
                            grid_y= y,
                            world=self.world,
                            id=f"Receiver-{t}",
                            process_strategy=process_strategy_c,
                            state={"address": ips[t]}
                        ))
                elif t == "-":
                    self.world.add_tile(EW_Wire(grid_x=x, grid_y=y, world=self.world))
                elif t == "|":
                    self.world.add_tile(NS_Wire(grid_x=x, grid_y=y, world=self.world))
                elif t == "^":
                    self.world.add_tile(NEW_Wire(grid_x=x, grid_y=y, world=self.world))

