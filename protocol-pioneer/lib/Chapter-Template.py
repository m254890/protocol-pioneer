from .Scenario import Scenario
from .model import *
from random import randint

from typing import Callable, List

class Chapter4(Scenario):

    def __init__(self,
                 user_strategy:Callable[[UserNode], None],
                 wait_for_user: bool = False
                 ) -> None:
        

        self.did_win = False
        self.did_lose = False

        def set_win(command) -> None:
            self.did_win = True

        def set_lose(i) -> None:
            self.did_lose = True
                    

        strategies = {
            "1": user_strategy,
            "2": user_strategy,
            "3": user_strategy,
            "4": user_strategy,
            "5": user_strategy,
            "6": user_strategy,
            "A": emitter_strategy
        }

        sprites = {
            "1": "assets/min/Drone-1.png",
            "2": "assets/min/Drone-1.png",
            "3": "assets/min/Drone-1.png",
            "4": "assets/min/Drone-1.png",
            "5": "assets/min/Drone-1.png",
            "6": "assets/min/Drone-1.png",
            "A": "assets/min/Damaged-Ship-1.png",
        }

        ids = {
            "1": "1",
            "2": "2",
            "3": "3",
            "4": "4",
            "5": "5",
            "6": "6",
            "A": "You",
        }

        # Load entities and tiles
        tile_map: List[str] = [
            "........6...",
            "........|...",
            "........|...",
            "....3---4...",
            "....|...|...",
            ".A--1---2...",
            "........|...",
            "........|...",
            "........5...",
            "............",
        ]
        
        # Create world
        super().__init__(
            world_grid_height=len(tile_map),
            world_grid_width=len(tile_map[0]),
            grid_pixel_size=50,
            wait_for_user=wait_for_user
        )

        for (y, row) in enumerate(tile_map):
            for (x, t) in enumerate(row):
                if t == ".":
                    pass
                elif t in "A123456":
                    self.world.add_entity(
                        DroneNode(
                            grid_x= x,
                            grid_y= y,
                            world=self.world,
                            id=ids[t],
                            label=ids[t],
                            process_strategy=strategies[t],
                            sprite_file=sprites[t]
                        ))
                elif t == "-":
                    self.world.add_tile(EW_Wire(grid_x=x, grid_y=y, world=self.world))
                elif t == "|":
                    self.world.add_tile(NS_Wire(grid_x=x, grid_y=y, world=self.world))
                elif t == "^":
                    self.world.add_tile(NEW_Wire(grid_x=x, grid_y=y, world=self.world))
        
    @staticmethod
    def introduce():
        print(
"""
    ____             __                   ______  _                           
   / __ \_________  / /_____  _________  / / __ \(_)___  ____  ___  ___  _____
  / /_/ / ___/ __ \/ __/ __ \/ ___/ __ \/ / /_/ / / __ \/ __ \/ _ \/ _ \/ ___/
 / ____/ /  / /_/ / /_/ /_/ / /__/ /_/ / / ____/ / /_/ / / / /  __/  __/ /    
/_/   /_/   \____/\__/\____/\___/\____/_/_/   /_/\____/_/ /_/\___/\___/_/
              
Chapter 4
""")

    def win(self) -> bool:
        return self.did_win
    
    def lose(self) -> bool:
        return self.did_lose
    
    def post_win(self) -> None:
        print(
"""
You win!
"""
        )
    
    def post_lose(self) -> None:
        print(
"""
You lose!
"""
        )