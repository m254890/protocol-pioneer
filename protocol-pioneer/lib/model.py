from dataclasses import dataclass, field
from ipycanvas import Canvas
from ipywidgets import Image
from typing import List, Tuple, Dict, Callable, Optional, Set
import random
import copy
from enum import Enum


@dataclass(kw_only=True)
class World:
    """Represents the world and is the interface between entities/tiles. It will know where everything in the world is
    and can give that information to anyone who asks."""

    # world parameters
    world_width: int # number of grid spaces in a row
    world_height: int # number of grid spaces in a column
    grid_size: int = 50 # in pixels
    canvas_padding: int = 0 # number of pixels to pad the canvas
    message_filter: Callable[[str], bool] = lambda m: True # a function that determines whether a message should be displayed

    def __post_init__(self):
        # Tiles are stored as a sparse array to make lookup more efficient. The key is a tuple (x,y).
        self.tiles: Dict[Tuple[int, int], "Tile"] = {}
        
        # Entities are stored as a simple List
        self.entities: List["Entity"] = []

        # Keep track of the simulation time step
        self.current_tick = 0

    def add_entity(self, entity: "Entity") -> None:
        self.entities.append(entity)

    def add_tile(self, tile: "Tile") -> None:
        self.tiles[(tile.grid_x, tile.grid_y)] = tile

    def tile_at(self, grid_x: int, grid_y: int) -> "Tile":
        """Returns a reference to a Blank Tile if nothing else exists."""
        try:
            return self.tiles[(grid_x, grid_y)]
        except KeyError:
            return Blank_Tile(grid_x=grid_x, grid_y=grid_y, world=self)

    def entities_at(self, grid_x: int, grid_y: int) -> List["Entity"]:
        """Returns an empty list of no entities exist at the requested coordinates."""
        return [e for e in self.entities if e.grid_x==grid_x and e.grid_y==grid_y]
    
    def remove_entity(self, entity: "Entity") -> None:
        """Removes the entity with the given id from the world."""
        self.entities.remove(entity)
    
    def draw_all(self, canvas: Canvas, animation_frame: float = 0.0) -> None:
        """Draws all tiles, then all entities.
        
        `animation_frame` is a value between 0 and 1 that determines how to place the
        object between grid_x/y and next_grid_x/y
        """
        
        # memoize the tiles canvas with non-movable Drawables
        if not hasattr(self, "tiles_canvas"):
            self.tiles_canvas = Canvas(width=self.grid_size * self.world_width, height=self.grid_size * self.world_height)
            for gy in range(self.world_height):
                for gx in range(self.world_width):
                    x = self.grid_to_pixel(gx)
                    y = self.grid_to_pixel(gy)
                    tile = self.tile_at(gx, gy)
                    tile.draw_at(canvas=self.tiles_canvas, pixel_x=x, pixel_y=y)
            # Now draw the non-movable entities
            for entity in self.entities:
                if not entity.movable:
                    x = self.grid_to_pixel(entity.grid_x)
                    y = self.grid_to_pixel(entity.grid_y)
                    entity.draw_at(canvas=self.tiles_canvas, pixel_x=x, pixel_y=y)

        canvas.draw_image(self.tiles_canvas, 0, 0, self.grid_size * self.world_width, self.grid_size * self.world_height)

        # Entities. Draw the movables.
        # Let's try to make this more efficient? If the same sprite was already drawn at the same coordinates, don't
        # draw it again.
        # sprites_drawn: Set[Tuple[int, int, str]] = set() # (x, y, classname)
        for entity in self.entities:
            # Skip EnrouteMessages if they don't pass the filter
            if isinstance(entity, EnrouteMessage) and not self.message_filter(entity.text):
                continue
            
            # Now only draw movable entities that haven't been drawn yet.
            if entity.movable:
                x = round((self.grid_to_pixel(entity.grid_x)) * (1-animation_frame) + (self.grid_to_pixel(entity.next_grid_x)) * animation_frame)
                y = round((self.grid_to_pixel(entity.grid_y)) * (1-animation_frame) + (self.grid_to_pixel(entity.next_grid_y)) * animation_frame)
                classname = entity.__class__.__name__
                # if (x, y, classname) not in sprites_drawn:
                    # sprites_drawn.add((x, y, classname))
                entity.draw_at(canvas=canvas, pixel_x=x, pixel_y=y)

    def process_all(self) -> None:
        """Calls the process method on all entities."""

        # This should be called once per tick, so let's take this opportunity to increment the time step
        self.current_tick += 1

        for e in copy.copy(self.entities):
            e.process()


    def animate_all(self, canvas: Canvas, percent: float) -> None:
        """Animates all entities between their current location and their next location. This frame,
        draw them `percent` of the way to their destination.
        
        Args:
            percent (float): A float from 0.0 - 1.0"""
        self.draw_all(canvas=canvas, animation_frame=percent)

    def post_animate_all(self):
        for entity in self.entities:
            entity.post_animate()

    def grid_to_pixel(self, grid_coord: int) -> int:
        """Converts a grid coordinate to a pixel coordinate"""
        return grid_coord * self.grid_size + self.canvas_padding
    
    def get_current_tick(self) -> int:
        return self.current_tick


@dataclass
class Drawable:
    """Tiles and Entities can both be drawn to the screen. We will use sprites in this game."""
    grid_x: int
    grid_y: int
    world: World
    label: Optional[str] = None
    movable: bool = True # if False, we can cache the sprite in the background.

    def __post_init__(self):
        self.frames: List[Canvas] = []
        self.current_frame: int = 0
        self.next_grid_x: int = self.grid_x
        self.next_grid_y: int = self.grid_y
        self.pixel_height: int = self.world.grid_size
        self.pixel_width: int = self.world.grid_size

    def add_sprite_frame(self, filename: str) -> None:
        temp_canvas = Canvas(width=self.pixel_width, height=self.pixel_height)
        img = Image.from_file(filename)
        temp_canvas.draw_image(img, 0, 0, self.pixel_width, self.pixel_height)
        
        self.frames.append(temp_canvas)

    def draw_at(self, canvas: Canvas, pixel_x: int, pixel_y: int) -> None:
        # iterate to next frame
        self.current_frame = (self.current_frame + 1) % len(self.frames)

        canvas.draw_image(self.frames[self.current_frame], pixel_x, pixel_y, self.pixel_width, self.pixel_height)

        # draw label
        if self.label:
            font_size = 12
            font_y: int = pixel_y + self.world.grid_size + round(font_size / 2)
            font_x: int = round(pixel_x + self.world.grid_size/4)

            #first draw a black box just big enough for the text
            # canvas.fill_style = "black"
            # canvas.fill_rect(font_x - 10, font_y - 10, 20, 20)
            canvas.font = f"{font_size}px serif"
            canvas.fill_style = "white"
            canvas.stroke_style = "white"
            canvas.text_align = "right"
            canvas.shadow_color = "black"
            
            canvas.fill_text(self.label, font_x, font_y)
 
##############################
## Entities
##############################

class Direction(Enum):
    """An enum class for representing directions"""
    NORTH = "NORTH"
    SOUTH = "SOUTH"
    EAST = "EAST"
    WEST = "WEST"
    NONE = "NONE"

    def opposite(self) -> "Direction":
        if self == Direction.NORTH:
            return Direction.SOUTH
        elif self == Direction.SOUTH:
            return Direction.NORTH
        elif self == Direction.EAST:
            return Direction.WEST
        elif self == Direction.WEST:
            return Direction.EAST
        else:
            raise ValueError(f"Invalid direction: {self}")

@dataclass
class Entity(Drawable):
    """Things that aren't tiles in the world. Sometimes they move, sometimes they emit messages. They have
    a logic step `process()`. Multiple entities can occupy the same space.
    
    In each "round", or "tick", there are 3 "phases":
    1. Process
    2. Animate (move)
    3. Post-animate

    At the beginning of the round, a moving Entity is at location (x,y). During the Process step it decides
    that it needs to move to (x+1, y). We want all the pieces to move at the same time, so we can't just animate and
    move this piece during that step. All the pieces need to undergo th Process stage before moving
    on to the animate stage. We have next_grid_x/y to keep track of where the Entity will move next turn (which
    it decided based on the process step.)

    The Animate stage is when we draw the moving sprites from their current location to their "next" grid
    coordinates. During this time we don't mutate self.grid_x/y or self.next_grid_x/y. We use those values
    (and a percent complete input) and interpolate to find the location to draw.

    The post-animate phase is where the current grid_x/y is mutated to next_grid_x/y.

    Here's another way to look at it:
    1. Process - decide next state
    2. Animate - show Entities animating between states
    3. Post-animate - move entities to new states.

    """

    id: str = ""
    direction: Direction = Direction.NONE

    def __post_init__(self):
        super().__post_init__()
        if self.id == "":
            self.id = f"{self.__class__.__name__}-{random.randint(0, 100000)}"

    def process(self):
        raise NotImplementedError("Nodes must implement process")

    def animate(self):
        raise NotImplementedError("Nodes must implement animate")

    def post_animate(self):
        raise NotImplementedError("Nodes must implement post_animate")


@dataclass(kw_only=True)
class EnrouteMessage(Entity):
    """This is a message in flight."""
    origin: Entity # the entity that sent this message. For informational purposes.
    text: str

    def __post_init__(self):
        super().__post_init__()
        self.add_sprite_frame("assets/min/Message-1.png")

    def draw_at(self, canvas: Canvas, pixel_x: int, pixel_y: int) -> None:
        return super().draw_at(canvas, pixel_x, pixel_y)

    def process(self):
        """Overriding the Entity process()method."""
        
        # Deliver message at nodes
        for entity in self.world.entities_at(self.grid_x, self.grid_y):
            if isinstance(entity, Node):
                # determine interface
                interface = "Error"
                if self.direction == Direction.NORTH:
                    interface = "S"
                elif self.direction == Direction.SOUTH:
                    interface = "N"
                elif self.direction == Direction.EAST:
                    interface = "W"
                elif self.direction == Direction.WEST:
                    interface = "E"
                # deliver message
                entity.receive_message(self, interface)

                # remove this message and return (I'm assuming there cannot be multiple nodes on
                # a single grid tile.)
                self.world.remove_entity(self)
                return

        # move the message if it wasn't delivered. The movement will depend on the tile the message is on.
        # a message on a blank tile is destroyed.
        tile = self.world.tile_at(grid_x=self.grid_x, grid_y=self.grid_y)

        if isinstance(tile, Blank_Tile):
            self.world.remove_entity(self)
            return

        if isinstance(tile, Wire):
            directions = [Direction.NORTH, Direction.SOUTH, Direction.EAST, Direction.WEST]
            exits = tile.exits_from_direction(self.direction.opposite())
            for direction in exits:
                m = self.copy()
                m.id = f"{m.id}{random.randint(0, 10)}"
                m.direction = direction
                m.next_grid_x, m.next_grid_y = self.grid_coordinates_in_direction(self.grid_x, self.grid_y, direction)
                self.world.add_entity(m)
            self.world.remove_entity(self)
            return
        
    
    def animate(self):
        return super().animate()
    
    def post_animate(self):
        self.grid_x = self.next_grid_x
        self.grid_y = self.next_grid_y

    def grid_coordinates_in_direction(self, grid_x, grid_y, direction: Direction) -> Tuple[int, int]:
        """Translates coordinates one grid box in the specified direction."""
        if direction == Direction.NORTH:
            return (grid_x, grid_y - 1)
        elif direction == Direction.SOUTH:
            return (grid_x, grid_y + 1)
        elif direction == Direction.EAST:
            return (grid_x + 1, grid_y)
        elif direction == Direction.WEST:
            return (grid_x - 1, grid_y)
        else:
            raise ValueError(f"Invalid direction: {direction}")

    def copy(self) -> "EnrouteMessage":
        return copy.copy(self)

@dataclass
class QueuedMessage:
    """A simple container for messages received at a Node.
    
    Args:
        text (str): The contents of the message
        interface (str): The interface that the message was received on. "N" means it came from the
            North interface, "W" indicates it came from the West interface.
    """
    text: str
    interface: str

@dataclass
class Node(Entity):
    """A Node can send and receive messages. They are the "agents" in this game. They can be hosts, clients,
    servers, routers, switches, hubs, etc.
    """

    sprite_file: str = "assets/min/Mothership1.png"

    def __post_init__(self):
        super().__post_init__()
        self.message_queue: List[QueuedMessage] = []
        self.add_sprite_frame(self.sprite_file)

    def send_message(self, message_text: str, interface: str) -> None:
        """Sends a message. It will create a message on an adjacent tile, in the
        direction of the interface.
        
        Args:
            message_text (str): The contents of the message
            interface (str): The interface to send the message on. Good values: "N", "S", "E", "W".
        """
        # figure out message coordinates and direction based on the interface
        if interface == "N":
            message_x = self.grid_x
            message_y = self.grid_y - 1
            direction = Direction.NORTH
        elif interface == "S":
            message_x = self.grid_x
            message_y = self.grid_y + 1
            direction = Direction.SOUTH
        elif interface == "E":
            message_x = self.grid_x + 1
            message_y = self.grid_y
            direction = Direction.EAST
        elif interface == "W":
            message_x = self.grid_x - 1
            message_y = self.grid_y
            direction = Direction.WEST
        else:
            raise ValueError(f"Invalid interface: {interface}")
            
        # create EnrouteMessage object    
        message = EnrouteMessage(grid_x=self.grid_x, grid_y=self.grid_y, world=self.world, 
                                origin=self, text=str(message_text), direction=direction)
        message.next_grid_x = message_x
        message.next_grid_y = message_y
        # Add the message to the world
        self.world.add_entity(message)


    def receive_message(self, message: EnrouteMessage, interface: str):
        """Called when a message arrives. Queues the message."""
        m = QueuedMessage(text=message.text, interface=interface)
        self.message_queue.append(m)


    def process(self):
        pass
    
    def animate(self):
        return super().animate()
    
    def post_animate(self):
        # I don't think there's anything for a node to do.
        pass


@dataclass(kw_only=True)
class DrawableText(Entity):
    """A text area."""
    label: str
    movable: bool = True
    process_strategy: Callable[["DrawableText"], None]

    def draw_at(self, canvas: Canvas, pixel_x: int, pixel_y: int) -> None:
        font_size = 20
        font_y: int = pixel_y + round(self.world.grid_size / 2)# + round(font_size / 2)
        font_x: int = pixel_x

        canvas.font = f"{font_size}px serif"
        canvas.fill_style = "white"
        canvas.stroke_style = "white"
        canvas.text_align = "left"
        canvas.shadow_color = "black"
        
        canvas.fill_text(self.label, font_x, font_y)

    def process(self):
        self.process_strategy(self)

    def post_animate(self):
        pass    

@dataclass(kw_only=True)
class UserNode(Node):
    """A node with a custom on_receive method."""
    process_strategy: Callable[["UserNode"], None]
    # A state variable the user can use to store values between clicks.
    state: Dict = field(default_factory=dict)
    movable: bool = False


    def __post_init__(self):
        super().__post_init__()

    def process(self):
        self.process_strategy(self)

    def connected_interfaces(self) -> List[str]:
        """Returns a list of the interfaces that are connected to wires."""
        interfaces = []
        
        if isinstance(self.world.tile_at(self.grid_x, self.grid_y - 1), Wire):
            interfaces.append("N")
        if isinstance(self.world.tile_at(self.grid_x, self.grid_y + 1), Wire):
            interfaces.append("S")
        if isinstance(self.world.tile_at(self.grid_x + 1, self.grid_y), Wire):
            interfaces.append("E")
        if isinstance(self.world.tile_at(self.grid_x - 1, self.grid_y), Wire):
            interfaces.append("W")

        return interfaces

##############################
## Tiles
##############################

@dataclass
class Tile(Drawable):
    """Tiles don't move, and they don't think. But they do influence the entities on top of them.
    They get drawn below the Entities, and probably remain static throughout the game."""
    movable: bool = False


@dataclass
class Blank_Tile(Tile):
    """A wire that runs east-west"""
    def __post_init__(self):
        super().__post_init__()
        s = random.choice([
            "assets/min/Space-1.png",
            "assets/min/Space-2.png",
            "assets/min/Space-3.png",
            "assets/min/Space-4.png",
            "assets/min/Space-5.png",
            "assets/min/Space-6.png",
        ])
        self.add_sprite_frame(s)

#
# Wires
#
class Wire(Tile):
    """Messages can move along wires"""
    
    def exits_from_direction(self, direction: Direction) -> List[Direction]:
        """Returns a list of the directions that a message will exit this wire if it enters from the
        specified direction."""
        return []

# Wires:
# NSEW (Four-way)
# NSE NSW NEW SEW
# NS NE NW SE SW EW

@dataclass
class NSE_Wire(Wire):
    def __post_init__(self):
        super().__post_init__()
        self.add_sprite_frame("assets/min/NSE-Wire.png")

    def exits_from_direction(self, direction: Direction) -> List[Direction]:
        exits = [Direction.NORTH, Direction.SOUTH, Direction.EAST]
        return [e for e in exits if e != direction]
    
@dataclass
class NSW_Wire(Wire):
    def __post_init__(self):
        super().__post_init__()
        self.add_sprite_frame("assets/min/NSW-Wire.png")

    def exits_from_direction(self, direction: Direction) -> List[Direction]:
        exits = [Direction.NORTH, Direction.SOUTH, Direction.WEST]
        return [e for e in exits if e != direction]
    
@dataclass
class NEW_Wire(Wire):
    def __post_init__(self):
        super().__post_init__()
        self.add_sprite_frame("assets/min/NEW-Wire.png")

    def exits_from_direction(self, direction: Direction) -> List[Direction]:
        exits = [Direction.NORTH, Direction.EAST, Direction.WEST]
        return [e for e in exits if e != direction]
    
@dataclass
class SEW_Wire(Wire):
    def __post_init__(self):
        super().__post_init__()
        self.add_sprite_frame("assets/min/SEW-Wire.png")

    def exits_from_direction(self, direction: Direction) -> List[Direction]:
        exits = [Direction.SOUTH, Direction.EAST, Direction.WEST]
        return [e for e in exits if e != direction]
    
@dataclass
class NS_Wire(Wire):
    """ ║ A wire that runs north-south"""
    def __post_init__(self):
        super().__post_init__()
        self.add_sprite_frame("assets/min/NS-Wire.png")

    def exits_from_direction(self, direction: Direction) -> List[Direction]:
        exits = [Direction.SOUTH, Direction.NORTH]
        return [e for e in exits if e != direction]
    
@dataclass
class NE_Wire(Wire):
    """ ╚ A wire with exits both east and north."""
    def __post_init__(self):
        super().__post_init__()
        self.add_sprite_frame("assets/min/NE-Wire.png")

    def exits_from_direction(self, direction: Direction) -> List[Direction]:
        exits = [Direction.NORTH, Direction.EAST]
        return [e for e in exits if e != direction]
    
@dataclass
class NW_Wire(Wire):
    """ ╝ A wire with exits both north and west."""
    def __post_init__(self):
        super().__post_init__()
        self.add_sprite_frame("assets/min/NW-Wire.png")

    def exits_from_direction(self, direction: Direction) -> List[Direction]:
        exits = [Direction.NORTH, Direction.WEST]
        return [e for e in exits if e != direction]
    
@dataclass
class SE_Wire(Wire):
    """ ╔ A wire with exits both east and south."""
    def __post_init__(self):
        super().__post_init__()
        self.add_sprite_frame("assets/min/SE-Wire.png")

    def exits_from_direction(self, direction: Direction) -> List[Direction]:
        exits = [Direction.SOUTH, Direction.EAST]
        return [e for e in exits if e != direction]
    
@dataclass
class SW_Wire(Wire):
    """ ╗ A wire with exits both south and west."""
    def __post_init__(self):
        super().__post_init__()
        self.add_sprite_frame("assets/min/SW-Wire.png")

    def exits_from_direction(self, direction: Direction) -> List[Direction]:
        exits = [Direction.SOUTH, Direction.WEST]
        return [e for e in exits if e != direction]
    
@dataclass
class EW_Wire(Wire):
    """A wire that runs east-west"""
    def __post_init__(self):
        super().__post_init__()
        self.add_sprite_frame("assets/min/EW-Wire.png")

    def exits_from_direction(self, direction: Direction) -> List[Direction]:
        exits = [Direction.WEST, Direction.EAST]
        return [e for e in exits if e != direction]
    
@dataclass
class NSEW_Wire(Wire):
    """A wire that runs in all directions."""
    def __post_init__(self):
        super().__post_init__()
        self.add_sprite_frame("assets/min/NSEW-Wire.png")

    def exits_from_direction(self, direction: Direction) -> List[Direction]:
        exits = [Direction.WEST, Direction.EAST, Direction.NORTH, Direction.SOUTH]
        return [e for e in exits if e != direction]