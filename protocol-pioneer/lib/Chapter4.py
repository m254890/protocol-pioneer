# pyright: reportInvalidStringEscapeSequence=false

from .Scenario import Scenario
from .model import *
from random import randint, shuffle

from typing import Callable, List

class Chapter4(Scenario):

    def __init__(self,
                 drone_strategy:Callable[[UserNode], None],
                 scanner_strategy:Callable[[UserNode], None],
                 wait_for_user: bool = False,
                 animation_frames: int = 10
                 ) -> None:
        
        # Load entities and tiles
        tile_map: List[str] = [
            "........A.",
            "........|.",
            ".1--L---R.",
            "....|...|.",
            ".2--M---Q.",
            "....|...|.",
            "..X.N---P.",
            "....|...|.",
            ".3--O...B.",
            "..........",
        ]

        self.did_win = False
        self.did_lose = False
        self.win_conditions = {
            "1.1": False,
            "1.2": False,
            "1.3": False,
        }
        self.lose_condition: str = ""

        def make_message(source, dest, command, value):
            return f"Source:{source}\nDest:{dest}\nCommand:{command}\nValue:{value}"
        
        def parse_message(message):
            """Parse a message into a dictionary."""
            lines = message.split("\n")
            d: Dict[str, str] = {}
            for line in lines:
                if line:
                    k, v = line.split(":")
                    d[k] = v
            return d

        def set_win(i) -> None:
            self.win_conditions[i] = True
            if all(self.win_conditions.values()):
                self.did_win = True

        def set_lose(i) -> None:
            self.lose_condition = i
            self.did_lose = True

        # I'll keep track of results received in the sender nodes because the success values
        # for each command will fall in certain ranges.
        # Boot values 100-199
        # Aim values 200-299
        # Scan values 300 and greater 
        success_number = randint(1,25)
        fail_number = randint(26,50)
        magic_number = randint(1,10)
        boot_number = 100
        aim_number = 200
        scan_number = 300

        @dataclass(kw_only=True)
        class ScannerNode(UserNode):

            def boot(self, value):
                # If value is a str, convert it to an int
                if isinstance(value, str):
                    value = int(value)
                return value + success_number

            def aim(self, value):
                # If value is a str, convert it to an int
                if isinstance(value, str):
                    value = int(value)
                return value + success_number

            def scan(self, value):
                # If value is a str, convert it to an int
                if isinstance(value, str):
                    value = int(value)
                return value + success_number


        
        # Commands are Boot, Aim, and Scan
        # Pair each sender node with a scanner drone
        friends = ["2.1", "2.2", "2.2"]
        friends_dict = {
            "1.1": friends[0],
            "1.2": friends[1],
            "1.3": friends[2],
        }
        shuffle(friends)
        def get_friend(i: str) -> str:
            """Returns the drone this ship is paired to."""
            return friends_dict[i]

        def client_strategy(self):

            if "booted" not in self.state:
                self.state["booted"] = 1
                self.send_message(make_message(self.id, get_friend(self.id), "Boot", boot_number+magic_number), "E")


            # Handle received messages
            while(self.message_queue):
                m = self.message_queue.pop()
                parsed = parse_message(m.text)
                if not parsed.get("Command", "error").lower() == "result":
                    print(f"{self.id}: Received bad command in message {m.text}. I only expect 'result' commands.")
                    continue
                
                src = parsed.get("Source", "error")
                if src == "error":
                    print(f"{self.id}: Malformed Source field in message {m.text}.")
                    continue
                if src != get_friend(self.id):
                    print(f"{self.id}: Who is {m.text}? Why is this drone talking to me? I only expect messages from {get_friend(self.id)}.")
                    continue

                v = parsed.get("Value", "error")
                if not v.isnumeric():
                    print(f"{self.id}: Received bad value in message {m.text}. I only expect numeric results.")
                else:
                    if int(v) < boot_number + 99:
                        if int(v) == (boot_number + magic_number + success_number):
                            print(f"{self.id}: <sci-fi power up sound> Boot successful! Proceeding to aim...")
                            self.send_message(make_message(self.id, get_friend(self.id), "Aim", aim_number+magic_number), "E")
                        elif int(v) == (boot_number + magic_number +  fail_number):
                            print(f"{self.id}: <sci-fi power down sound> Boot failed! Must try again...")
                            self.send_message(make_message(self.id, get_friend(self.id), "Boot", boot_number+magic_number), "E")
                        else:
                            print(f"{self.id}: Received bad value {v}.")
                    elif int(v) < aim_number + 99:
                        if int(v) == (aim_number + magic_number +  success_number):
                            print(f"{self.id}: <sci-fi robot laser sounds> Aim successful! Proceeding to scan...")
                            self.send_message(make_message(self.id, get_friend(self.id), "Scan", scan_number+magic_number), "E")
                        elif int(v) == (aim_number + magic_number +  fail_number):
                            print(f"{self.id}: <sci-fi robot grinding sounds> Aim failed! What's happening??")
                            self.send_message(make_message(self.id, get_friend(self.id), "Aim", aim_number+magic_number), "E")
                        else:
                            print(f"{self.id}: Received bad value {v}.")
                    elif int(v) > 300 and int(v) < 399:
                        print(f"{self.id}: Received result. Unsatisfied. Must search deeper...")
                        self.send_message(make_message(self.id, get_friend(self.id), "Scan", scan_number+randint(1000,10000)), "E")
                    if int(v) > 400:
                        print(f"{self.id}: Received result. Satisfied. Intrigued. Reporting to Mother.")
                        set_win(self.id)
                        
            # Count messages in flight. If more than 7, print a warning. If more than 10, the player loses.
            messages_in_flight = len([x for x in self.world.entities if isinstance(x, EnrouteMessage)])
            if messages_in_flight > 7:
                print("Warning: High number of messages are interfering with drone sensors! Overload imminent!")
            if messages_in_flight > 10:
                print("Critical Failure: Drone sensors overloaded! Critical Failure!")
                set_lose("too_many_messages")

        # Observer strategy
        def observer_strategy(self) -> None:
            pass

        strategies = {
            "1": client_strategy,
            "2": client_strategy,
            "3": client_strategy,
            "A": scanner_strategy,
            "B": scanner_strategy,
            "L": drone_strategy,
            "M": drone_strategy,
            "N": drone_strategy,
            "O": drone_strategy,
            "P": drone_strategy,
            "Q": drone_strategy,
            "R": drone_strategy,
            "X": observer_strategy,
        }

        sprites = {
            "1": "assets/min/Scanner-Ship-1.png",
            "2": "assets/min/Scanner-Ship-1.png",
            "3": "assets/min/Scanner-Ship-1.png",
            "A": "assets/min/Telescope-1.png",
            "B": "assets/min/Telescope-1.png",
            "L": "assets/min/Drone-1.png",
            "M": "assets/min/Drone-1.png",
            "N": "assets/min/Drone-1.png",
            "O": "assets/min/Drone-1.png",
            "P": "assets/min/Drone-1.png",
            "Q": "assets/min/Drone-1.png",
            "R": "assets/min/Drone-1.png",
            "X": "assets/min/Damaged-Ship-1.png",
        }

        ids = {
            "1": "1.1",
            "2": "1.2",
            "3": "1.3",
            "A": "2.1",
            "B": "2.2",
            "L": "3.1",
            "M": "3.2",
            "N": "3.3",
            "O": "3.4",
            "P": "3.5",
            "Q": "3.6",
            "R": "3.7",
            "X": "You",
        }
        
        # Create world
        super().__init__(
            world_grid_height=len(tile_map),
            world_grid_width=len(tile_map[0]),
            grid_pixel_size=50,
            wait_for_user=wait_for_user,
            animation_frames=animation_frames
        )

        for (y, row) in enumerate(tile_map):
            for (x, t) in enumerate(row):
                if t == ".":
                    pass
                elif t in "123":
                    self.world.add_entity(
                        UserNode(
                            grid_x= x,
                            grid_y= y,
                            world=self.world,
                            id=ids[t],
                            label=f"Analyzer {ids[t]}",
                            process_strategy=strategies[t],
                            sprite_file=sprites[t]
                        ))
                elif t in "LMNOPQR":
                    self.world.add_entity(
                        UserNode(
                            grid_x= x,
                            grid_y= y,
                            world=self.world,
                            id=ids[t],
                            label=f"Drone {ids[t]}",
                            process_strategy=strategies[t],
                            sprite_file=sprites[t]
                        ))
                elif t in "X":
                    self.world.add_entity(
                        UserNode(
                            grid_x= x,
                            grid_y= y,
                            world=self.world,
                            id=ids[t],
                            label=f"You",
                            process_strategy=strategies[t],
                            sprite_file=sprites[t]
                        ))
                elif t in "AB":
                    self.world.add_entity(
                        ScannerNode(
                            grid_x= x,
                            grid_y= y,
                            world=self.world,
                            id=ids[t],
                            label=f"Scanner {ids[t]}",
                            process_strategy=strategies[t],
                            sprite_file=sprites[t]
                        ))
                elif t == "-":
                    self.world.add_tile(EW_Wire(grid_x=x, grid_y=y, world=self.world))
                elif t == "|":
                    self.world.add_tile(NS_Wire(grid_x=x, grid_y=y, world=self.world))
                elif t == "⅃":
                    self.world.add_tile(NW_Wire(grid_x=x, grid_y=y, world=self.world))
        
    @staticmethod
    def introduce():
        print(
"""
    ____             __                   ______  _                           
   / __ \_________  / /_____  _________  / / __ \(_)___  ____  ___  ___  _____
  / /_/ / ___/ __ \/ __/ __ \/ ___/ __ \/ / /_/ / / __ \/ __ \/ _ \/ _ \/ ___/
 / ____/ /  / /_/ / /_/ /_/ / /__/ /_/ / / ____/ / /_/ / / / /  __/  __/ /    
/_/   /_/   \____/\__/\____/\___/\____/_/_/   /_/\____/_/ /_/\___/\___/_/
              
Chapter 4: Double Trouble

As the last Ratchet crew member exits, Sawblade hesitates at the threshold. She looks back, her gaze sweeping over the battered room, lingering on the flickering screen, and finally settling on you and Jansen. She steps back inside, the door closing behind her with a soft thud.

"I've got a gut feeling there's more to be done here. And my gut's got a better track record than most," she says, her voice steady, eyes reflecting the silent resolve of a seasoned warrior. "So, I’m staying."

"Oh, ok," says Jansen as he messes with his jacket zipper.

The drones outside are now accompanied by new, more powerful deep space scanners. Analyzer ships loom in the distance. You glance at Jansen and watch the room's reflection dance in his glasses.

Jansen clears his throat, "So, uh, these drones... they're just routers now, right? And the new scanners will handle the actual scanning?"

You nod, "Exactly. And we've got to program them separately. It's not just about routing messages anymore; it’s a two-tier system now. The scanners need to understand the commands, execute them, and then respond back to the Analyzers."

The weight of reality sets on Jansen's face. He leans closer to the screen, eyes flicking over the network schematics. "A bit more on our plate, but I guess that's progress," he mumbles, and starts typing.
""")

    def win(self) -> bool:
        return self.did_win
    
    def lose(self) -> bool:
        return self.did_lose
    
    def post_win(self) -> None:
        print(
"""
As the final data packets make their return journey from the scanners to the analyzers, tiny green symbols line up on the screen, indicating successful deliveries.

Jansen leans back, a sigh of relief escaping his lips as he rubs his temples, "If I ever see another routing table, I'm going to - what's this?"

He points at the message from Analyzer 1.2, "Received result. Satisfied. Intrigued. Reporting to Mother.” His brows furrow, "Intrigued? What does that even mean?"

You shrug, though the Analyzer's message does send a slight chill down your spine. "Maybe it's just a weird way of saying 'mission accomplished'."

But before the thought fully settles, a sudden, ominous alert blares from the console. The screen flickers, displaying a series of erratic ID switches among the drones. The once stable identifiers are now shuffling in a chaotic dance, the neat lines of code jumbling into a frenzied scramble on the screen.

"What in the...?" Jansen's voice trembles as he stares at the ever-shifting IDs, the well-structured network now a dissonant cacophony of signals.

The drone identifiers continue to morph, each attempt to stabilize them only adding to the chaos. The once smooth stream of data now a turbulent storm threatening to tear apart the fragile veil of control you had over the network.

Sawblade steps closer to the console, her face hardened but eyes betraying a trace of fear. “I... wish I knew how to use a computer," she whispers to herself.

Your fingers race over the keys in a desperate attempt to restore order, but the system seems to have a mind of its own. 

Just then, a shattering explosion rocks the ship. The console before you sparks and shudders, the world around you trembling as a deafening roar envelops everything.

“Grab the suits!” you scream over the cacophony, the reality of what's happening barely registering.

In a whirl of action, the three of you scramble to snatch the emergency low pressure suits from their compartments. Smoke stings your eyes as you struggle to put on the suit. You glance at Jansen, his hands shaking as he fumbles with the zipper.

"Jansen!" you yell. He looks up, his eyes wide with fear.

You reach out to help when a second explosion hurls you into darkness.

~~ Continue to Chapter 5 ~~
"""
        )
    
    def post_lose(self) -> None:
        if self.lose_condition == "too_many_messages":
            print(
"""
The pace of commands quickens. Fear thrums. The screen before you is a storm, messages lost in the digital chaos. You glance at Jansen, his face ashen. The drones' sensor readings soar.

A shrill alarm slices the room. Drone 3.1's sensors max out. A digital meltdown begins. "No, no, no..." Jansen mutters, cutting communication links. But it's too late. One by one, drones shut down. Each failure ripples through the network, heightening the overload.

Drone 3.3 nears meltdown. A massive energy surge races back through the link. The room lights up, circuits fry.

You're thrown back, world ablaze. The last sound before darkness is metal screeching and Jansen's scream.

GAME OVER
"""
        )