# pyright: reportInvalidStringEscapeSequence=false

from .Scenario import Scenario
from .model import *

from typing import Callable, List

class Chapter2(Scenario):

    def __init__(self,
                 user_strategy:Callable[[UserNode], None],
                 wait_for_user: bool = False,
                 animation_frames: int = 10
                 ) -> None:
        

        # I'll track the win condition using a member variable,
        # and create a set_win function to use in a closure below.
        self.win_condition = 0
        self.did_win = False
        self.did_lose = False

        def set_win() -> None:
            """We need all 3 rescue ships to report success 3 times. So, a total of 9."""
            self.win_condition += 1
            if self.win_condition == 3:
                self.did_win = True

        def set_lose() -> None:
            self.did_lose = True

        # Observer strategy
        def observer_strategy(self) -> None:
            pass

        # Emitter node strategy
        def rescue_strategy(self, msg_direction: str) -> None:
            """This "Emitter" node emits 3 messages. The messages are a random number from 0 to 10.
            The win condition is that it receives a message with the sum of all the numbers.
            """
            
            # initialize state
            if "clicks" not in self.state:
                self.state["clicks"] = 0
            if "sum" not in self.state:
                self.state["sum"] = 0
            if "msg_count" not in self.state:
                self.state["msg_count"] = 0
            
            if random.randint(0,1) == 1 and self.state["msg_count"] < 3:
                n = random.randint(0,10)
                self.state["sum"] += n
                text = str(n)
                interface = msg_direction
                self.send_message(text, interface)
                self.state["msg_count"] += 1

            # handle received message
            while(self.message_queue):
                m: QueuedMessage = self.message_queue.pop()
                if self.state["msg_count"] < 3:
                    print(f"{self.id} - received message when not expecting one! {m.text}")
                    set_lose()
                else:
                    if str(m.text) == str(self.state["sum"]):
                        print(f"{self.id} - calibration successful!")
                        set_win()
                    else:
                        print(f"{self.id} - calibration failed! Received message {m.text} but expected {self.state['sum']}")
                        set_lose()
                    

        strategies = {
            "A": lambda self: rescue_strategy(self, "E"),
            "C": lambda self: rescue_strategy(self, "N"),
            "D": lambda self: rescue_strategy(self, "S"),
            "E": observer_strategy,
            "B": user_strategy
        }

        sprites = {
            "A": "assets/min/Repair-Ship-1.png",
            "C": "assets/min/Repair-Ship-1.png",
            "D": "assets/min/Repair-Ship-1.png",
            "E": "assets/min/Mothership1.png",
            "B": "assets/min/Damaged-Ship-1.png",
        }

        ids = {
            "A": "Ratchet 01",
            "C": "Ratchet 02",
            "D": "Ratchet 03",
            "E": "Mothership Aether",
            "B": "You"
        }

        # Load entities and tiles
        tile_map: List[str] = [
            "......D.....",
            ".E....|.....",
            "......|.....",
            ".A----B.....",
            "......|.....",
            "......|.....",
            "......C.....",
            "............",
        ]

        # Create world
        super().__init__(
            world_grid_height=len(tile_map),
            world_grid_width=len(tile_map[0]),
            grid_pixel_size=50,
            wait_for_user=wait_for_user,
            animation_frames=animation_frames
        )

        for (y, row) in enumerate(tile_map):
            for (x, t) in enumerate(row):
                if t == ".":
                    pass
                elif t in "ABCDE":
                    self.world.add_entity(
                        UserNode(
                            grid_x= x,
                            grid_y= y,
                            world=self.world,
                            id=ids[t],
                            label=ids[t],
                            process_strategy=strategies[t],
                            sprite_file=sprites[t]
                        ))
                elif t == "-":
                    self.world.add_tile(EW_Wire(grid_x=x, grid_y=y, world=self.world))
                elif t == "|":
                    self.world.add_tile(NS_Wire(grid_x=x, grid_y=y, world=self.world))
                elif t == "^":
                    self.world.add_tile(NEW_Wire(grid_x=x, grid_y=y, world=self.world))
        
    @staticmethod
    def introduce():
        print(
"""
    ____             __                   ______  _                           
   / __ \_________  / /_____  _________  / / __ \(_)___  ____  ___  ___  _____
  / /_/ / ___/ __ \/ __/ __ \/ ___/ __ \/ / /_/ / / __ \/ __ \/ _ \/ _ \/ ___/
 / ____/ /  / /_/ / /_/ /_/ / /__/ /_/ / / ____/ / /_/ / / / /  __/  __/ /    
/_/   /_/   \____/\__/\____/\___/\____/_/_/   /_/\____/_/ /_/\___/\___/_/
              
Chapter 2: Hope in the Darkness

"Ratchet oh-one marks target at one-three-alpha"

"Roger oh-one, continue inbound."

The radio chirps along as the rescue effort proceeds. You've set the comms panel to the rescue frequency to ease your nerves. Jansen sits in the seat next to you, holding a cup of tea he made out of the last of the water. You're glad he's here.

A new voice cuts through the static. It's from the Mothership Aether, "Attention, survivors. We have dispatched three repair ships to assist. As they approach, they will calibrate their instruments by sending sets of numerical data. You will play a crucial role in this calibration."

Jansen raises an eyebrow, "Sounds fancy."

Before you can comment, a different, gruffer voice jumps in, "Hey there, it's Ratchet oh-two. Let me break it down for ya, alright? Each of us repair ships – me, oh-one, and oh-three – will be sending ya three numbers, one by one. Once you get all three from any one of us, add 'em up and shoot the total back. That'll help us tune our gear right. Got it?"

You glance at Jansen. He nods, scribbling something down. "So," he says, "if Ratchet oh-three sends... 5, 3, and 4, we send back 12?"

"Spot on," comes the response, this time from a voice you guess to be Ratchet oh-three, "And remember, sum the numbers for each ship separately. Don't lump us all together. We're particular about our calibrations!"

The Mothership Aether chimes in, "Do ensure accuracy in this process. It's crucial for the success of the rescue mission."

Your fingers hover over the comms panel. This just became an interactive waiting game, and every number counts.
""")

    def win(self) -> bool:
        return self.did_win
    
    def lose(self) -> bool:
        return self.did_lose
    
    def post_win(self) -> None:
        print(
"""
After sending the final calculated number, a tense few seconds of silence ensues. You hold your breath, your eyes locked onto the flashing green light of the transmitted message.

Suddenly, a cheerful, rough-and-ready voice breaks the silence, "Spot on! Ratchet oh-two calibrated and good to go!"

Ratchet oh-one chimes in with a sigh of relief, "Lock established! Gear's humming perfectly now."

Jansen lets out a whoop of joy, "We did it!"

From the static, the confident voice of Ratchet oh-three confirms, "That's a wrap, folks! Instrumentation's dialed in and we're ready for the assist."

The comms are then graced by the smooth, refined tone of the Mothership Aether, "Excellent work, survivors. Thanks to your precision, the rescue operation can proceed without impediments. Remain in place and await further instructions."

The room fills with a palpable sense of hope, the dim lighting now seemingly brighter. Jansen leans back in his seat, a grin splitting his face, "Guess we're getting out of this one, eh?"

You nod, smiling back. "Guess so."
"""
        )
    
    def post_lose(self) -> None:
        print(
"""
The comms panel remains quiet for an uncomfortably long moment after you send the total. The ambient noise of distant static grates on your nerves, making the wait even more tense.

Suddenly, a sharp crackle rips through the silence, followed by the concerned voice of the Mothership Aether, "Survivor, the data you transmitted doesn't align with our calibration expectations. Please confirm—"

Before the voice can finish, a much gruffer interruption comes from Ratchet oh-two, "Damn it! We're gettin' interference here! Whatever number ya sent just threw our systems haywire!"

Another voice adds, their tone a mix of frustration and urgency, "We're veering off course! Can't get a lock!"

From the corner of your eye, you see Jansen gripping his seat, his face pale. "We might've messed this up," he murmurs, glancing at you with regret.

Suddenly, a loud siren blares from the control panel, and the ship lurches violently. The red emergency lights flare up, casting an ominous glow throughout the room.

The last voice you hear is Ratchet oh-three, voice filled with despair, "We're going down! Mayday! Mayday!"

Darkness engulfs everything.

[GAME OVER - TRY AGAIN?]
"""
        )