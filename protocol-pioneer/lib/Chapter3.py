# pyright: reportInvalidStringEscapeSequence=false

from .Scenario import Scenario
from .model import *
from random import randint

from typing import Callable, List

class Chapter3(Scenario):

    def __init__(self,
                 user_strategy:Callable[[UserNode], None],
                 wait_for_user: bool = False,
                 animation_frames: int = 10
                 ) -> None:
        

        # I'll track the win condition using a member variable,
        # and create a set_win function to use in a closure below.
        self.win_condition = 0
        self.did_win = False
        self.lose_condition = 0 # 1-wrong command, 2-too many messages in flight
        self.messages_to_send = [
                f"Dest:4\nCommand:start_emit\nValue:{randint(0,100)}",
                f"Dest:2\nCommand:start_emit\nValue:{randint(0,100)}",
                f"Dest:5\nCommand:start_emit\nValue:{randint(0,100)}",
                f"Dest:6\nCommand:start_emit\nValue:{randint(0,100)}",
                f"Dest:2\nCommand:focus\nValue:{randint(0,100)}",
                f"Dest:5\nCommand:focus\nValue:{randint(0,100)}",
                f"Dest:2\nCommand:return_results\nValue:{randint(0,100)}",
            ]

        def set_win(command) -> None:
            """We need all the messages to be received."""
            # print(f"Messages to send: {self.messages_to_send}")
            # print(f"Command received: {command}")
            if command in self.messages_to_send:
                print("Success!")
                self.win_condition += 1
            else:
                print("Critical Failure!")
                self.lose_condition = 1
            if self.win_condition == len(self.messages_to_send):
                self.did_win = True

        def set_lose(i) -> None:
            self.lose_condition = i


        def get_message_to_send(i):
            return self.messages_to_send[i]
        
        @dataclass(kw_only=True)
        class DroneNode(UserNode):
            """This node includes custom functions that the player is supposed to
            call based on messages received from the emitter ship."""
            def start_emit(self, value):
                print(f"Drone {self.id}: Bleep bloop. Emit sequence started. Target: {value}...", end="")
                set_win(f"Dest:{self.id}\nCommand:start_emit\nValue:{value}")

            def focus(self, value):
                print(f"Drone {self.id}: Bleep bloop. Focusing to level {value}...", end="")
                set_win(f"Dest:{self.id}\nCommand:focus\nValue:{value}")

            def return_results(self, value):
                print(f"Drone {self.id}: Bleep bloop. Returning results. Value: {value}...", end="")
                set_win(f"Dest:{self.id}\nCommand:return_results\nValue:{value}")

        # Emitter node strategy
        def emitter_strategy(self) -> None:
            """Emits a series of messages."""

            if "messages_sent" not in self.state:
                self.state["messages_sent"] = 0
            
            if randint(0,1) == 0 and self.state["messages_sent"] < 7:
                m = get_message_to_send(self.state["messages_sent"])
                self.state["messages_sent"] += 1
                self.send_message(m, "E")

            # Count messages in flight. If more than 7, print a warning. If more than 10, the player loses.
            messages_in_flight = len([x for x in self.world.entities if isinstance(x, EnrouteMessage)])
            if messages_in_flight > 7:
                print("Warning: High number of messages are interfering with drone sensors! Overload imminent!")
            if messages_in_flight > 10:
                print("Critical Failure: Drone sensors overloaded! Critical Failure!")
                set_lose(2)
                    

        strategies = {
            "1": user_strategy,
            "2": user_strategy,
            "3": user_strategy,
            "4": user_strategy,
            "5": user_strategy,
            "6": user_strategy,
            "A": emitter_strategy
        }

        sprites = {
            "1": "assets/min/Drone-1.png",
            "2": "assets/min/Drone-1.png",
            "3": "assets/min/Drone-1.png",
            "4": "assets/min/Drone-1.png",
            "5": "assets/min/Drone-1.png",
            "6": "assets/min/Drone-1.png",
            "A": "assets/min/Damaged-Ship-1.png",
        }

        ids = {
            "1": "1",
            "2": "2",
            "3": "3",
            "4": "4",
            "5": "5",
            "6": "6",
            "A": "You",
        }

        # Load entities and tiles
        tile_map: List[str] = [
            "........6...",
            "........|...",
            "........|...",
            "....3---4...",
            "....|...|...",
            ".A--1---2...",
            "........|...",
            "........|...",
            "........5...",
            "............",
        ]
        
        # Create world
        super().__init__(
            world_grid_height=len(tile_map),
            world_grid_width=len(tile_map[0]),
            grid_pixel_size=50,
            wait_for_user=wait_for_user,
            animation_frames=animation_frames
        )

        for (y, row) in enumerate(tile_map):
            for (x, t) in enumerate(row):
                if t == ".":
                    pass
                elif t in "A123456":
                    self.world.add_entity(
                        DroneNode(
                            grid_x= x,
                            grid_y= y,
                            world=self.world,
                            id=ids[t],
                            label=ids[t],
                            process_strategy=strategies[t],
                            sprite_file=sprites[t]
                        ))
                elif t == "-":
                    self.world.add_tile(EW_Wire(grid_x=x, grid_y=y, world=self.world))
                elif t == "|":
                    self.world.add_tile(NS_Wire(grid_x=x, grid_y=y, world=self.world))
                elif t == "^":
                    self.world.add_tile(NEW_Wire(grid_x=x, grid_y=y, world=self.world))
        
    @staticmethod
    def introduce():
        print(
"""
    ____             __                   ______  _                           
   / __ \_________  / /_____  _________  / / __ \(_)___  ____  ___  ___  _____
  / /_/ / ___/ __ \/ __/ __ \/ ___/ __ \/ / /_/ / / __ \/ __ \/ _ \/ _ \/ ___/
 / ____/ /  / /_/ / /_/ /_/ / /__/ /_/ / / ____/ / /_/ / / / /  __/  __/ /    
/_/   /_/   \____/\__/\____/\___/\____/_/_/   /_/\____/_/ /_/\___/\___/_/
              
Chapter 3: Strings in the Shadows

The hum of approaching ships fills the room. You lean forward in anticipation, a grin forming on your face. "Finally," you say, watching as the silhouettes of the 'rescue' ships come into view.

Jansen's eyes squint in suspicion. "Those are smaller than I expected. Even for a rescue."

From the radio, the familiar gruff voice of Ratchet oh-two cuts in, "Alright, folks, dropping off your lifeline package. These drones will be your eyes and ears out there."

You glance at Jansen. "Lifeline package? I thought these ships were our ticket out of here."

Ratchet oh-three chuckles over the comms, "With these bellies? Room for one and one only. But don't worry, these drones are top of the line. They'll help you... well, whatever you need them for."

Ratchet oh-one adds, "Think of it as a, uh, DIY rescue."

As the drones attach to the ship's exterior, a smooth voice from the Mothership Aether fills the room, "Survivor, these drones are imperative for our next steps. An attack on our fleet is no small matter. We need to uncover the culprits."

You glance at Jansen. He looks worried. Your glasses begin to fog.

Aether continues: "Your mission is to use these drones to explore the surrounding sector. They will act as relay nodes, receiving and forwarding messages. Your ship will be the primary sender. But remember, each drone has limited range. You'll need to program them to route the messages accurately."

You raise an eyebrow, taking in the information, "So it's a game of space hopscotch?"

"Precisely. There are six drones in total. If a message needs to reach Drone 4 from your ship, you might send it to Drone 1, which sends it to Drone 3, and then onto Drone 4. It's all about finding the right pathway."

You look at the network topology on the screen. Six nodes, interconnected, each with its distinct interfaces. The screen hums quietly, waiting for input.
""")

    def win(self) -> bool:
        return self.did_win
    
    def lose(self) -> bool:
        return self.lose_condition > 0
    
    def post_win(self) -> None:
        print(
"""
As you meticulously guide the drones through their pathways, the feedback lights on the console begin to flash in an erratic pattern. The six nodes on the screen create a network pattern that suddenly vibrates, showing a strong resonance.

"That... wasn't expected," Jansen mutters, leaning closer to inspect the readings.

You nod in agreement, "The drones detected something. But what?"

A garbled signal, layered with static, floods the comms. It's neither a message from the Mothership Aether nor from any of the Ratchet pilots. The frequency is unfamiliar, yet it bears a strange rhythmic pattern, as if something or someone is trying to communicate.

Before either of you can make sense of the signal, a distant thudding noise echoes through the ship. Jansen grips the armrest, eyes wide, "Did you hear that?"

The ship's internal door slides open slowly. Shadows move in the dim light, growing clearer as figures step into the comms room. It's the Ratchet crew, each clad in their distinctive repair gear, tools in hand.

A woman wearing a nametag emblazoned "Sawblade" raises her hand in a calming gesture, says, "Easy there. We picked up on those signals too. Figured it's best we get this bird back in shape sooner rather than later."

A heavy-set man with fluorescent cargo pants adds, as he swings a hefty wrench over his shoulder, "Yeah, and besides, a ship in the dark is no good. Time to shine some light on the situation."

You exhale, the tension in the room palpably dropping, "So, you're here to help?"

Sawblade nods, "Always. Let's get this ship operational."

Jansen chuckles, squeaking his chair as he spins it around idly with his foot.

The room fills with the comforting sounds of work in progress and the promise of clarity on the mysterious signals that lie ahead.
"""
        )
    
    def post_lose(self) -> None:
        if self.lose_condition == 1: # wrong command
            print(
"""
The command keys clack under your fingers, each stroke echoing through the tense silence of the comms room. You punch in the final command for Drone 5, but a tremor of doubt shakes your resolve. Something doesn’t feel right, but time is a luxury you can’t afford. You hit ‘send’.

The drones hum in the void, processing your commands. A sudden, sharp beep pierces the silence. An error message flashes on the screen, “Incorrect Command: Drone 5.”

Your heart sinks. The room begins to spin. “No,” you whisper, "no, no, no!" You frantically try to override the command, but it’s too late. The drone misinterprets the command and fires its thrusters wildly.

Beside you, Jansen’s face turns ghost white. His hand slips into yours.

The rogue drone spirals towards the station, its thrusters burning bright in the dark abyss. The impact is deafening. A shockwave rips through the station, tearing metal, shattering glass.

Game over.
"""
        )
        elif self.lose_condition == 2: # too many messages in flight
            print(
"""
The pace of commands quickens. Fear thrums. The screen before you is a storm, messages lost in the digital chaos. You glance at Jansen, his face ashen. The drones' sensor readings soar.

A shrill alarm slices the room. Drone 3's sensors max out. A digital meltdown begins. "No, no, no..." Jansen mutters, cutting communication links. But it's too late. One by one, drones shut down. Each failure ripples through the network, heightening the overload.

Drone 1 nears meltdown. A massive energy surge races back through the link. The room lights up, circuits fry.

You're thrown back, world ablaze. The last sound before darkness is metal screeching and Jansen's scream.

Game over.
"""
        )