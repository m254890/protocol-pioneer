![Protocol Pioneer](/head.png)

<figure>
<img src="/screenshot.gif" width="512px">
<img src="/screenshot2.gif" width="512px">
</figure>

# Protocol Pioneer

A game about space, love, and computer networks.

## Installation

**Create a Virtual Environment**:
   Navigate to your project directory in a terminal or command prompt, and run:
   ```bash
   python3 -m venv myenv
   ```
   Replace `myenv` with your preferred name for the virtual environment.

**Activate the Virtual Environment**:
- **On macOS and Linux**:
    ```bash
    source myenv/bin/activate
    ```
- **On Windows** (use Command Prompt, not PowerShell):
    ```bash
    .\myenv\Scripts\activate
    ```

Your prompt should now show the activated environment name (e.g., `(myenv)`).

**Install Required Packages**:
   With the virtual environment active, run:
   ```bash
   pip install -r requirements.txt
   ```

**Run JupyterLab**:
   Start JupyterLab with:
   ```bash
   jupyter-lab
   ```

## Playing the Game

⚠️ This is important! In JupyterLab, navigate to the `protocol-pioneer` folder. The structure looks something like:

```
protocol-pioneer
├── assets
│   └── ...
├── lib
│   ├── model.py
│   ├── Scenario.py
│   ├── ...
├── chapters
│   ├── Chapter-01.ipynb  <- To play Chapter 1, copy this file
│   ├── Chapter-02.ipynb
│   ├── ...
│
├─ Chapter-01-m091890.ipynb  <- To here. Then open it and run the notebook.
```

To start Chapter 1, copy `protocol-pioneer/chapters/Chapter-01.ipynb` to `protocol-pioneer/Chapter-01-m091890.ipynb`. This way if I update the `Chapter-01.ipynb` file in the Gitlab repo, you can pull the changes without conflicting with your solutions.

## Pulling Updates

I will be making regular updates to the game. If you've cloned and solved some of the puzzles already, you can just `pull` the changes.

First, commit your changes (save your local work):
```bash
git add .
git commit -m "My work on Chapter 1"
```

Then pull the changes:
```bash
git pull
```
